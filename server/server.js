const fs = require('fs');
const http = require('http');
const pug = require('pug');
const bodyParser = require('body-parser');
const crypto = require('crypto');
const moment = require('moment-timezone');

const express = require('express');
const app = express();

const path = require('path');
const { nextTick, exit } = require('process');
const { v4: uuidv4 } = require('uuid');

const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost/concours", {useNewUrlParser: true});


const db = mongoose.connection;
const usersSchema = new mongoose.Schema({id:String, name:String, password:String, salt:String, token:String, dateValidite:Date, score:Number, responses:Object});
const Users = mongoose.model('users', usersSchema);

const anecdotesSchema = new mongoose.Schema({id:String, descriptif:String, veracite:String, auteur:String});
const Anecdotes = mongoose.model('anecdotes', anecdotesSchema);


var methodOverride = require("method-override");
app.use(methodOverride("_method"));

const port = 3000;

app.use(express.urlencoded({
    extended: true
}));


app.set('view engine', 'pug');

app.get('/', (req, res) => {
    res.render('login');
});
app.get('/inscription', (req, res) => {
    res.render('register');
});

app.get('/recupererToutesAnecdotes', (req, res) => {
    res.render('getAllAnecdotes');
});
app.get('/voirScores', (req, res) => {
    res.render('getScores');
});

app.get('/ajouterAnecdote', (req, res) => {
    res.render('addAnecdote');
});
app.get('/repondre', (req, res) => {
    res.render('answer');
});
app.get('/modifier', (req, res) => {
    res.render('edit');
});



/**
 * @api {post} /getSalt Recuperer le salt d'un utilisateur. S'il n'existe pas, en genere un autre
 * @apiPermission tout le monde
 *
 * @apiParam (Request body) {String} login : Le nom de l'utilisateur
 *
 * @apiExample {js} Example usage:
 * const data = {
 *   "login": "JohnDoe"
 * }
 *
 * @apiSuccessExample {json} Success response:
 *     HTTPS 200 OK
 *     {
 *      "salt": "420BlazeIt",
 *    }
 */
app.post('/getSalt', bodyParser.json(), (req, res, next) => {
    Users.find({ name:req.body.name }, function(err, arr) {
        if (arr.length === 1) {
            res.status(200).send({"salt" : arr[0].salt});
        } else {
            newSalt = crypto.randomBytes(8).toString("base64");
            res.status(200).send({"newSalt" : newSalt});
        }
    })
});



/**
 * @api {post} /login Se connecter avec login + mdp pour recuperer le token
 * @apiPermission tout le monde
 *
 * @apiParam (Request body) {String} login : Le nom de l'utilisateur
 * @apiParam (Request body) {String} password : Le mdp de l'utilisateur (hash et salt)
 *
 * @apiExample {js} Example usage:
 * const data = {
 *   "login": "JohnDoe",
 *   "password": "$2y$10$MjjGKnhzXE0i4JzcpUVyzOYD0sXUpSseqyFnl0xVACQdjWny4T0w2"
 * }
 *
 * @apiSuccessExample {json} Success response:
 *     HTTPS 200 OK
 *     {
 *      "token": "4504281014783341941515187074114105",
 *    }
 */
app.post('/login', bodyParser.json(), (req, res, next) => {
    Users.find({ name:req.body.login, password:req.body.password }, function(err, arr) {
        if (arr.length === 1) {
            const dateActuelle = Date.now();
            const dateExpiration = moment(dateActuelle).add(30, 'm').toDate();
            const tokenGenere = generateToken(req.body.name, dateActuelle);

            Users.updateOne({name:req.body.login },  { dateValidite: dateExpiration, token:tokenGenere }, function (err, result)  {
                if (err) {
                    return console.error(err);
                }
                if (result.nModified === 0) {
                    res.status(500).send({"erreur": "Probleme durant la mise a jour du token, veuillez réessayer plus tard"})
                    return
                }
                res.status(200).send({"token" : tokenGenere});
            });
        } else {
            res.status(200).send({"erreur" : "Login invalide"});
        }
    });
});


/**
 * @api {get} /getAllAnecdotes Recuperer toutes les anecdotes existantes
 * @apiPermission Utilisateur avec un token
 *
 * @apiParam {String} token : Le token (valide) de l'utilisateur
 *
 * @apiExample {js} Example usage:
 * const data = {
 *   "token": "4504281014783341941515187074114105",
 * }
 *
 * @apiSuccessExample {json} Success response:
 *     HTTPS 200 OK
 *     {
 *      [
 *           {
 *               "_id": "60b911df2f50a20aec0863fc",
 *               "id": "1",
 *               "descriptif": "Dans les salles machines du master en 2009, il fallait mettre des bâches pour protéger les machines les jours de forte pluie"
 *           },
 *           {
 *               "_id": "60b911df2f50a20aec0863fd",
 *               "id": "2",
 *               "descriptif": "Chaque année les M2 ICE font une pièce de théâtre. En 2021, elle aura lieu comme à son habitude dans la fabrique sur le campus du Mirail "
 *           }
 *       ]
 *    }
 */
app.get('/getAllAnecdotes', (req, res, next) => {
    let tokenOk = false;
    Users.find({ token:req.query.token }, function(err, arr) {
        tokenOk = arr.length === 1 && new Date(Date.parse(arr[0].dateValidite)) >= new Date();
    }).then( () => {
        if (tokenOk) {
            // ========== UPDATE DATE VALIDITE TOKEN ===================================================================
            const dateActuelle = Date.now();
            const dateExpiration = moment(dateActuelle).add(30, 'm').toDate();
            Users.updateOne({token:req.query.token },  { dateValidite: dateExpiration }, function (err, result)  {
                if (err) {
                    return console.error(err);
                }
            });
            // =========================================================================================================
            // Le 2eme argument sert a exclure des champs
            Anecdotes.find({}, {_id:0, veracite:0, __v:0}, function (err, anecdotes) {
                if (err) {
                    return console.error(err);
                }
                /*const generatedTemplate = compiledFunction({
                    data: Anecdotes,
                })
                res.send(generatedTemplate)*/
                res.status(200).send(anecdotes);
                //}
            });
        } else {
            res.status(200).send({"erreur" : "Connexion invalide"});
        }
        /*
        const generatedTemplate = compiledFunction({
            data: Anecdotes,
        })
        res.send(generatedTemplate)
        res.status(200).send(anecdotes);
         */
    });
});


/**
 * @api {post} /getAnecdotes Recuperer une anecdote
 * @apiPermission Utilisateur avec un token
 *
 * @apiParam {String} token : Le token (valide) de l'utilisateur
 *
 * @apiParam (Request body) {String} id : L'ID de l'anecdote a modifier
 *
 * @apiExample {js} Example usage:
 * const data = {
 *   "token": "4504281014783341941515187074114105",
 *   "id": "42"
 * }
 *
 * @apiSuccessExample {json} Success response:
 *     HTTPS 200 OK
 *     {
*          "_id": "60b911df2f50a20aec0863fc",
*          "id": "1",
*          "descriptif": "Dans les salles machines du master en 2009, il fallait mettre des bâches pour protéger les machines les jours de forte pluie"
*      }
 */
app.post('/getAnecdote', bodyParser.json(), (req, res, next) => {
    let tokenOk = false;
    Users.find({ token:req.body.token }, function(err, arr) {
        tokenOk = arr.length === 1 && new Date(Date.parse(arr[0].dateValidite)) >= new Date();
    }).then( () => {
        if (tokenOk) {
            // ========== UPDATE DATE VALIDITE TOKEN ===================================================================
            const dateActuelle = Date.now();
            const dateExpiration = moment(dateActuelle).add(30, 'm').toDate();
            Users.updateOne({token:req.body.token },  { dateValidite: dateExpiration }, function (err, result)  {
                if (err) {
                    return console.error(err);
                }
            });
            // =========================================================================================================
            // Le 2eme argument sert a exclure des champs
            Anecdotes.find({id:req.body.id}, {_id:0, veracite:0, __v:0}, function (err, anecdotes) {
                if (err) {
                    return console.error(err);
                }
                /*const generatedTemplate = compiledFunction({
                    data: Anecdotes,
                })
                res.send(generatedTemplate)*/
                res.status(200).send(anecdotes[0]);
                return;
                //}
            });
        } else {
            res.status(200).send({"erreur" : "Connexion invalide"});
        }
        /*
        const generatedTemplate = compiledFunction({
            data: Anecdotes,
        })
        res.send(generatedTemplate)
        res.status(200).send(anecdotes);
         */
    });
});


/**
 * @api {get} /getBestScore Recuperer le meilleur score
 * @apiPermission Utilisateur avec un token
 *
 * @apiParam {String} token : Le token (valide) de l'utilisateur
 *
 * @apiExample {js} Example usage:
 * const data = {
 *   "token": "4504281014783341941515187074114105",
 * }
 *
 * @apiSuccessExample {json} Success response:
 *     HTTPS 200 OK
 *     [
 *       {
 *         "name": "baptiste",
 *         "score": 2
 *       },
 *       {
 *         "name": "smain",
 *         "score": 2
 *       }
 *     ]
 */
app.get('/getBestScore', (req, res, next) => {
    let tokenOk = false;
    Users.find({ token:req.query.token }, function(err, arr) {
        tokenOk = arr.length === 1 && Date.parse(arr[0].dateValidite) >= Date.now();
    }).then( () => {
        if (tokenOk) {
            // ========== UPDATE DATE VALIDITE TOKEN ===================================================================
            const dateActuelle = Date.now();
            const dateExpiration = moment(dateActuelle).add(30, 'm').toDate();
            Users.updateOne({token: req.query.token}, {dateValidite: dateExpiration}, function (err, result) {
                if (err) {
                    return console.error(err);
                }
            });
            // =========================================================================================================
            Users.find(function (err, users) {
                if (err) {
                    return console.error(err);
                }
                let bestUsers = [];
                let userScore = 0;
                for (let u of users) {
                    if (u.score > userScore) {
                        userScore = u.score;
                        bestUsers = [{"name": u.name, "score": userScore}];
                    }else if(u.score == userScore){
                        bestUsers.push({"name": u.name, "score": userScore});
                    }
                }
                res.statusCode = 200;
                res.send(bestUsers);
            })
        } else {
            res.status(200).send({"erreur" : "Connexion invalide"});
        }
    });
});


/**
 * @api {post} /modifyAnecdote Modifier une anecdote
 * @apiPermission Utilisateur avec un token
 *
 * @apiParam (Request body) {String} token : Le token (valide) de l'utilisateur
 *
 * @apiParam (Request body) {String} id : L'ID de l'anecdote a modifier
 *
 * @apiParam (Request body) {String} descriptif : Texte de l'anecdote
 *
 * @apiParam (Request body) {Boolean} veracite : Veracite de l'anecdote
 *
 * @apiExample {js} Example usage:
 * const data = {
 *   "token": "4504281014783341941515187074114105",
 *   "id": "42",
 *   "descriptif": "J'ai deja fait planter tous les PC d'une salle",
 *   "veracite": true
 * }
 *
 * @apiSuccessExample {json} Success response:
 *     HTTPS 200 OK
 *     {
 *      "reponse" : "Anecdote modifiee"
 *    }
 */
app.post('/modifyAnecdote', bodyParser.json(), (req, res, next) => {
    let tokenOk = false;
    Users.find({ token:req.body.token }, function(err, arr) {
        tokenOk = arr.length === 1 && Date.parse(arr[0].dateValidite) >= Date.now();
    }).then( () => {
        if (tokenOk) {
            // ========== UPDATE DATE VALIDITE TOKEN ===================================================================
            const dateActuelle = Date.now();
            const dateExpiration = moment(dateActuelle).add(30, 'm').toDate();
            Users.updateOne({token: req.body.token}, {dateValidite: dateExpiration}, function (err, result) {
                if (err) {
                    return console.error(err);
                }
            });
            // =========================================================================================================
            const result = Anecdotes.updateOne({id: req.body.id}, {
                descriptif: req.body.descriptif,
                veracite: req.body.veracite
            }, function (err, ressult) {
                if (ressult.nModified === 0) {
                    res.status(200).send({"reponse": "L'id de l'anecdote n'existe pas"});
                    return
                }
                res.statusCode = 200;
                res.send({"reponse": "Anecdote modifiée"});
            });
        } else {
            res.status(200).send({"reponse" : "Connexion invalide"});
        }
    });
});


/**
 * @api {post} /deleteAnecdote Supprimer une anecdote
 * @apiPermission Utilisateur avec un token
 *
 * @apiParam (Request body) {String} token : Le token (valide) de l'utilisateur
 *
 * @apiParam (Request body) {String} id : L'ID de l'anecdote a supprimer
 *
 * @apiExample {js} Example usage:
 * const data = {
 *   "token": "4504281014783341941515187074114105",
 *   "id": "42",
 * }
 *
 * @apiSuccessExample {json} Success response:
 *     HTTPS 200 OK
 *     {
 *      "reponse" : "Anecdote supprimee"
 *    }
 */
app.post('/deleteAnecdote', bodyParser.json(),(req, res, next) => {

    Users.find({ token:req.body.token }, function(err, arr) {
        tokenOk = arr.length === 1 && Date.parse(arr[0].dateValidite) >= Date.now();
    }).then( () => {
        if (tokenOk) {
            // ========== UPDATE DATE VALIDITE TOKEN ===================================================================
            const dateActuelle = Date.now();
            const dateExpiration = moment(dateActuelle).add(30, 'm').toDate();
            Users.updateOne({token: req.body.token}, {dateValidite: dateExpiration}, function (err, result) {
                if (err) {
                    return console.error(err);
                }
            });
            // =========================================================================================================
            Anecdotes.deleteOne({ id: req.body.id }, function (err, mongooseDeleteResult) {
                if (err) return console.error(err);
                if (mongooseDeleteResult.deletedCount === 0) {
                    res.status(200).send({"reponse" : "L'id de l'anecdote n'existe pas"});
                    return
                }
                res.statusCode = 200;
                res.send({"reponse" : "Anecdote supprimée"});
            });
        } else {
            res.status(200).send({"reponse" : "Connexion invalide"});
        }
    });
});


/**
 * @api {post} /addAnecdote Ajouter une anecdote
 * @apiPermission Utilisateur avec un token
 *
 * @apiParam (Request body) {String} token : Le token (valide) de l'utilisateur
 *
 * @apiParam (Request body) {String} descriptif : Texte de l'anecdote
 *
 * @apiParam (Request body) {Boolean} veracite : Veracite de l'anecdote
 *
 * @apiExample {js} Example usage:
 * const data = {
 *   "token": "4504281014783341941515187074114105",
 *   "descriptif": "Le cours de Génie Logiciel de I. OBER est le même que celui dispensé à Paul Sabatier par sa femme, I. OBER",
 *   "veracite": true,
 * }
 *
 * @apiSuccessExample {json} Success response:
 *     HTTPS 200 OK
 *     {
 *      "reponse" : "Anecdote ajoutée"
 *    }
 */
app.post('/addAnecdote', bodyParser.json(), (req, res, next) => {
    let tokerOk = false;
    Users.find({ token:req.body.token }, function(err, arr) {
        tokenOk = arr.length === 1 && Date.parse(arr[0].dateValidite) >= Date.now();
    }).then( () => {
        if (tokenOk) {
            // ========== UPDATE DATE VALIDITE TOKEN ===================================================================
            const dateActuelle = Date.now();
            const dateExpiration = moment(dateActuelle).add(30, 'm').toDate();
            Users.updateOne({token: req.body.token}, {dateValidite: dateExpiration}, function (err, result) {
                if (err) {
                    return console.error(err);
                }
            });
            // =========================================================================================================
            const anecdote = new Anecdotes({id:uuidv4(), descriptif:req.body.descriptif, veracite:req.body.veracite});
            anecdote.save(function (err, result) {
                if (err) {
                    res.status(500).send({"reponse": "impossible d'ajouter l'anecdote"});
                    return console.error(err)
                }
                res.status(200).send({"reponse": "Anecdote ajoutée"});
            });
        } else {
            res.status(200).send({"erreur" : "Connexion invalide"});
        }
    });

});

/**
 * @api {post} /answer Repondre a une anecdote
 * @apiPermission Utilisateur avec un token
 *
 * @apiParam (Request body) {String} token : Le token (valide) de l'utilisateur
 *
 * @apiParam (Request body) {String} resId : Id de l'annecdote a repondre
 *
 * @apiParam (Request body) {Boolean} res : Reponse a l'anecdote
 *
 * @apiExample {js} Example usage:
 * const data = {
 *   "token": "4504281014783341941515187074114105",
 *   "resId": "98a9639b-9374-48c3-8219-dda6193ed09f",
 *   "res": true,
 * }
 *
 * @apiSuccessExample {json} Success response:
 *     HTTPS 200 OK
 *     {
 *      "reponse" : "Bonne reponse"
 *    }
 */
app.post('/answer', bodyParser.json(), (req, res, next) => {
    let tokenOk = false;
    Users.find({ token:req.body.token }, function(err, arr) {
        tokenOk = arr.length === 1 && Date.parse(arr[0].dateValidite) >= Date.now();
        if (tokenOk) {
            // ========== UPDATE DATE VALIDITE TOKEN ===================================================================
            const dateActuelle = Date.now();
            const dateExpiration = moment(dateActuelle).add(30, 'm').toDate();
            Users.updateOne({token: req.body.token}, {dateValidite: dateExpiration}, function (err, result) {
                if (err) {
                    return console.error(err);
                }
            });
            // =========================================================================================================
            
            Users.findOne({ id: arr[0].id }, function(err, arrU) {
                if (err) {
                    console.log(err)
                    return console.error(err)
                }
                if (arrU.length === 0) {
                    res.status(500).send({"erreur": "Utilisateur introuvable"})
                    return 
                }else{
                    let bonneRep = false
                    Anecdotes.find({ id: req.body.resId }, function(err, arrA) {
                        if (err) {
                            console.log(err)
                            return console.error(err)
                        }
                        if (arrA.length === 0) {
                            res.status(500).send({"erreur": "Anecdote introuvable"})
                            return 
                        }
                        if (arrA[0].veracite == req.body.res) {
                            bonneRep = true
                        }
        
                        let responses = arrU.responses
        
                        if (responses == null) {
                            responses = {}
                        }
                        
                        if (responses[req.body.resId] != null){
                            res.status(200).send({"reponse": "Vous avez deja repondu a cette question"})
                            return 
                        }
                        responses[req.body.resId] = req.body.res
                        let score = arrU.score
                        if (bonneRep) {
                            score += 1
        
                        }
        
                        Users.updateOne({ id: arr[0].id }, { responses: responses, score: score }, function (err, result) {
                            if (err) {
                                console.log(err)
                                return console.error(err)
                            }
                            if (result.nModified === 0) {
                                res.status(500).send({"erreur": "Probleme durant l'appréciation, veuillez réessayer plus tard"})
                                return
                            }else{
                                if (bonneRep) {
                                    res.statusCode = 200
                                    res.send({"reponse": "Bonne reponse"})
                                }else{
                                    res.statusCode = 200
                                    res.send({"reponse": "Mauvaise reponse"})
                                }
                                
                            }
        
                            
                        });
        
                    });
        
                    
                }
            });

        } else {
            res.status(200).send({"erreur" : "Connexion invalide"});
        }
    });


    
});

/**
 * @api {post} /register S'enregistrer avec login + mdp
 * @apiPermission tout le monde
 *
 * @apiParam (Request body) {String} name : Le nom de l'utilisateur
 * @apiParam (Request body) {String} password : Le mdp de l'utilisateur (hash et salt)
 * @apiParam (Request body) {String} salt : Le salt utilise par le client
 *
 * @apiExample {js} Example usage:
 * const data = {
 *   "name": "JohnDoe",
 *   "password": "$2y$10$MjjGKnhzXE0i4JzcpUVyzOYD0sXUpSseqyFnl0xVACQdjWny4T0w2",
 *   "salt": "Jo94TxpK",
 * }
 *
 * @apiSuccessExample {json} Success response:
 *     HTTPS 200 OK
 *     {
 *      "token": "4504281014783341941515187074114105",
 *    }
 */
app.post('/register', bodyParser.json(), (req, res, next) => {
    const dateActuelle = Date.now();
    const dateExpiration = moment(dateActuelle).add(30, 'm').toDate();

    const tokenGenere = generateToken(req.body.name, dateActuelle);


    Users.find({ name:req.body.name }, function(err, arr) {
        if (arr.length !== 0) {
           res.status(200).send({"erreur" : "Cet utilisateur existe déjà !"});
        } else {
            const user = new Users({id:uuidv4(), name:req.body.name, password:req.body.password, salt:req.body.salt, token:tokenGenere, dateValidite:dateExpiration, score:0});

            user.save(function (err, result) {
                if (err) {
                    console.log(err);
                }
            });

            res.status(200).send({"token" : tokenGenere});
        }
    });
});


/**
 * @api {get} /getMyScore Recuperer le score du joueur connecté
 * @apiPermission Utilisateur avec un token
 *
 * @apiParam {String} token : Le token (valide) de l'utilisateur
 *
 * @apiExample {js} Example usage:
 * const data = {
 *   "token": "4504281014783341941515187074114105",
 * }
 *
 * @apiSuccessExample {json} Success response:
 *     HTTPS 200 OK
 *     [
 *       {
 *         "score": "2"
 *       }
 *     ]
 */
 app.get('/getMyScore', (req, res, next) => {
    let tokenOk = false;
    Users.findOne({ token:req.query.token }, function(err, arr) {
        tokenOk =  Date.parse(arr.dateValidite) >= Date.now();
        if (tokenOk) {
            // ========== UPDATE DATE VALIDITE TOKEN ===================================================================
            const dateActuelle = Date.now();
            const dateExpiration = moment(dateActuelle).add(30, 'm').toDate();
            Users.updateOne({token: req.query.token}, {dateValidite: dateExpiration}, function (err, result) {
                if (err) {
                    return console.error(err);
                }
            });
            // =========================================================================================================
            res.status(200).send({"score" : arr.score, "name" : arr.name});
        } else {
            res.status(200).send({"erreur" : "Connexion invalide"});
        }

    });
});


app.use(express.static(path.join(__dirname, 'image')));

app.listen(port, () => {
    console.log(`serveur running at port : ${port}`);
});


/**
 *
 * @param login : nom d'utilisateur
 * @param date : number epoch representant la date de generation du token
 *
 * @return String : Le token de l'utilisateur
 */
function generateToken(login, date) {
    const data = login + date.toString();
    return crypto.createHash('md5').update(data).digest('hex');
}
